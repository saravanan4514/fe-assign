import React, { Component } from 'react';
import { connect } from 'react-redux';

let jobsList = (list, index) => {
  return (
    <div>
      <h3>{list.role}</h3>
      <p>{list.description}</p>
    </div>
  );
};

const Job = (props) => {
  let jobResults;
  if (props.searchJobs instanceof Array) {
    let results = props.searchJobs;
    jobResults = results.map(jobsList);
  }
  return (
    <div>
      {jobResults}
    </div>
  );
}

Job.displayName = 'Job';

export default Job;