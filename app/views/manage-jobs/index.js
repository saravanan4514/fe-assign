import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination } from 'antd';
import Job from '../components/job';

class ManageJobs extends Component {
  constructor () {
    super ();
    this.state = {
      current: 1
    };
    this.onPageChange = (value) => this._onPageChange(value);
  }

  _onPageChange (value) {
    console.log(value);
    this.setState({
      current: value
    }); 
  }
  render() {
    return (
      <div>
        <h2>Results</h2>
        <Job searchJobs={this.props.searchJobs} />
        <Pagination defaultCurrent={this.state.current} total={50} />
      </div>
    );
  }
}

ManageJobs.displayName = 'ManageJobs';

ManageJobs.defaultProps = {
  searchJobs: []
};

function select (state) {
  return {
    searchJobs: state.searchJob
  };
}

export default connect(select)(ManageJobs);
